/**
 *                                  Apache License
 *                            Version 2.0, January 2004
 *                         http://www.apache.org/licenses/
 *
 *    TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
 *
 *    1. Definitions.
 *
 *       "License" shall mean the terms and conditions for use, reproduction,
 *       and distribution as defined by Sections 1 through 9 of this document.
 *
 *       "Licensor" shall mean the copyright owner or entity authorized by
 *       the copyright owner that is granting the License.
 *
 *       "Legal Entity" shall mean the union of the acting entity and all
 *       other entities that control, are controlled by, or are under common
 *       control with that entity. For the purposes of this definition,
 *       "control" means (i) the power, direct or indirect, to cause the
 *       direction or management of such entity, whether by contract or
 *       otherwise, or (ii) ownership of fifty percent (50%) or more of the
 *       outstanding shares, or (iii) beneficial ownership of such entity.
 *
 *       "You" (or "Your") shall mean an individual or Legal Entity
 *       exercising permissions granted by this License.
 *
 *       "Source" form shall mean the preferred form for making modifications,
 *       including but not limited to software source code, documentation
 *       source, and configuration files.
 *
 *       "Object" form shall mean any form resulting from mechanical
 *       transformation or translation of a Source form, including but
 *       not limited to compiled object code, generated documentation,
 *       and conversions to other media types.
 *
 *       "Work" shall mean the work of authorship, whether in Source or
 *       Object form, made available under the License, as indicated by a
 *       copyright notice that is included in or attached to the work
 *       (an example is provided in the Appendix below).
 *
 *       "Derivative Works" shall mean any work, whether in Source or Object
 *       form, that is based on (or derived from) the Work and for which the
 *       editorial revisions, annotations, elaborations, or other modifications
 *       represent, as a whole, an original work of authorship. For the purposes
 *       of this License, Derivative Works shall not include works that remain
 *       separable from, or merely link (or bind by name) to the interfaces of,
 *       the Work and Derivative Works thereof.
 *
 *       "Contribution" shall mean any work of authorship, including
 *       the original version of the Work and any modifications or additions
 *       to that Work or Derivative Works thereof, that is intentionally
 *       submitted to Licensor for inclusion in the Work by the copyright owner
 *       or by an individual or Legal Entity authorized to submit on behalf of
 *       the copyright owner. For the purposes of this definition, "submitted"
 *       means any form of electronic, verbal, or written communication sent
 *       to the Licensor or its representatives, including but not limited to
 *       communication on electronic mailing lists, source code control systems,
 *       and issue tracking systems that are managed by, or on behalf of, the
 *       Licensor for the purpose of discussing and improving the Work, but
 *       excluding communication that is conspicuously marked or otherwise
 *       designated in writing by the copyright owner as "Not a Contribution."
 *
 *       "Contributor" shall mean Licensor and any individual or Legal Entity
 *       on behalf of whom a Contribution has been received by Licensor and
 *       subsequently incorporated within the Work.
 *
 *    2. Grant of Copyright License. Subject to the terms and conditions of
 *       this License, each Contributor hereby grants to You a perpetual,
 *       worldwide, non-exclusive, no-charge, royalty-free, irrevocable
 *       copyright license to reproduce, prepare Derivative Works of,
 *       publicly display, publicly perform, sublicense, and distribute the
 *       Work and such Derivative Works in Source or Object form.
 *
 *    3. Grant of Patent License. Subject to the terms and conditions of
 *       this License, each Contributor hereby grants to You a perpetual,
 *       worldwide, non-exclusive, no-charge, royalty-free, irrevocable
 *       (except as stated in this section) patent license to make, have made,
 *       use, offer to sell, sell, import, and otherwise transfer the Work,
 *       where such license applies only to those patent claims licensable
 *       by such Contributor that are necessarily infringed by their
 *       Contribution(s) alone or by combination of their Contribution(s)
 *       with the Work to which such Contribution(s) was submitted. If You
 *       institute patent litigation against any entity (including a
 *       cross-claim or counterclaim in a lawsuit) alleging that the Work
 *       or a Contribution incorporated within the Work constitutes direct
 *       or contributory patent infringement, then any patent licenses
 *       granted to You under this License for that Work shall terminate
 *       as of the date such litigation is filed.
 *
 *    4. Redistribution. You may reproduce and distribute copies of the
 *       Work or Derivative Works thereof in any medium, with or without
 *       modifications, and in Source or Object form, provided that You
 *       meet the following conditions:
 *
 *       (a) You must give any other recipients of the Work or
 *           Derivative Works a copy of this License; and
 *
 *       (b) You must cause any modified files to carry prominent notices
 *           stating that You changed the files; and
 *
 *       (c) You must retain, in the Source form of any Derivative Works
 *           that You distribute, all copyright, patent, trademark, and
 *           attribution notices from the Source form of the Work,
 *           excluding those notices that do not pertain to any part of
 *           the Derivative Works; and
 *
 *       (d) If the Work includes a "NOTICE" text file as part of its
 *           distribution, then any Derivative Works that You distribute must
 *           include a readable copy of the attribution notices contained
 *           within such NOTICE file, excluding those notices that do not
 *           pertain to any part of the Derivative Works, in at least one
 *           of the following places: within a NOTICE text file distributed
 *           as part of the Derivative Works; within the Source form or
 *           documentation, if provided along with the Derivative Works; or,
 *           within a display generated by the Derivative Works, if and
 *           wherever such third-party notices normally appear. The contents
 *           of the NOTICE file are for informational purposes only and
 *           do not modify the License. You may add Your own attribution
 *           notices within Derivative Works that You distribute, alongside
 *           or as an addendum to the NOTICE text from the Work, provided
 *           that such additional attribution notices cannot be construed
 *           as modifying the License.
 *
 *       You may add Your own copyright statement to Your modifications and
 *       may provide additional or different license terms and conditions
 *       for use, reproduction, or distribution of Your modifications, or
 *       for any such Derivative Works as a whole, provided Your use,
 *       reproduction, and distribution of the Work otherwise complies with
 *       the conditions stated in this License.
 *
 *    5. Submission of Contributions. Unless You explicitly state otherwise,
 *       any Contribution intentionally submitted for inclusion in the Work
 *       by You to the Licensor shall be under the terms and conditions of
 *       this License, without any additional terms or conditions.
 *       Notwithstanding the above, nothing herein shall supersede or modify
 *       the terms of any separate license agreement you may have executed
 *       with Licensor regarding such Contributions.
 *
 *    6. Trademarks. This License does not grant permission to use the trade
 *       names, trademarks, service marks, or product names of the Licensor,
 *       except as required for reasonable and customary use in describing the
 *       origin of the Work and reproducing the content of the NOTICE file.
 *
 *    7. Disclaimer of Warranty. Unless required by applicable law or
 *       agreed to in writing, Licensor provides the Work (and each
 *       Contributor provides its Contributions) on an "AS IS" BASIS,
 *       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *       implied, including, without limitation, any warranties or conditions
 *       of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
 *       PARTICULAR PURPOSE. You are solely responsible for determining the
 *       appropriateness of using or redistributing the Work and assume any
 *       risks associated with Your exercise of permissions under this License.
 *
 *    8. Limitation of Liability. In no event and under no legal theory,
 *       whether in tort (including negligence), contract, or otherwise,
 *       unless required by applicable law (such as deliberate and grossly
 *       negligent acts) or agreed to in writing, shall any Contributor be
 *       liable to You for damages, including any direct, indirect, special,
 *       incidental, or consequential damages of any character arising as a
 *       result of this License or out of the use or inability to use the
 *       Work (including but not limited to damages for loss of goodwill,
 *       work stoppage, computer failure or malfunction, or any and all
 *       other commercial damages or losses), even if such Contributor
 *       has been advised of the possibility of such damages.
 *
 *    9. Accepting Warranty or Additional Liability. While redistributing
 *       the Work or Derivative Works thereof, You may choose to offer,
 *       and charge a fee for, acceptance of support, warranty, indemnity,
 *       or other liability obligations and/or rights consistent with this
 *       License. However, in accepting such obligations, You may act only
 *       on Your own behalf and on Your sole responsibility, not on behalf
 *       of any other Contributor, and only if You agree to indemnify,
 *       defend, and hold each Contributor harmless for any liability
 *       incurred by, or claims asserted against, such Contributor by reason
 *       of your accepting any such warranty or additional liability.
 *
 *    END OF TERMS AND CONDITIONS
 *
 *    APPENDIX: How to apply the Apache License to your work.
 *
 *       To apply the Apache License to your work, attach the following
 *       boilerplate notice, with the fields enclosed by brackets "[]"
 *       replaced with your own identifying information. (Don't include
 *       the brackets!)  The text should be enclosed in the appropriate
 *       comment syntax for the file format. We also recommend that a
 *       file or class name and description of purpose be included on the
 *       same "printed page" as the copyright notice for easier
 *       identification within third-party archives.
 *
 *    Copyright 2021 徐浪 1053618636@qq.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package mydataharbor.plugin.app.task;

import mydataharbor.IDataPipline;
import mydataharbor.IDataSinkCreator;
import mydataharbor.IExecutorListener;
import mydataharbor.datasource.AbstractRateLimitDataSource;
import mydataharbor.datasource.RateLimitConfig;
import mydataharbor.executor.AbstractDataExecutor;
import mydataharbor.monitor.TaskExecutorMonitor;
import mydataharbor.plugin.api.IPluginInfoManager;
import mydataharbor.plugin.api.IPluginServer;
import mydataharbor.plugin.api.ITaskManager;
import mydataharbor.plugin.api.exception.PiplineCreateException;
import mydataharbor.plugin.api.exception.TaskManageException;
import mydataharbor.plugin.api.task.SingleTask;
import mydataharbor.plugin.api.task.TaskState;
import mydataharbor.plugin.app.listener.ExecutorListener;
import mydataharbor.setting.BaseSettingContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.framework.CuratorFramework;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @auth xulang
 * @Date 2021/6/30
 **/
@Slf4j
public class TaskManager implements ITaskManager {

  private IPluginInfoManager pluginInfoManager;

  private IPluginServer pluginServer;

  private CuratorFramework client;

  public TaskManager(IPluginInfoManager pluginInfoManager, CuratorFramework client, IPluginServer pluginServer) {
    this.pluginInfoManager = pluginInfoManager;
    this.pluginServer = pluginServer;
    this.client = client;
  }

  /**
   * pipline缓存
   */
  private Map<String, List<IDataPipline>> piplineMap = new ConcurrentHashMap<>();

  /**
   * 任务状态
   */
  private Map<String, TaskState> taskStateMap = new ConcurrentHashMap<>();

  /**
   * 所有提交的task
   */
  private volatile Map<String, SingleTask> taskMap = new ConcurrentHashMap<>();

  /**
   * executor缓存
   */
  private Map<String, List<AbstractDataExecutor>> executorMap = new ConcurrentHashMap<>();

  private Map<String, IExecutorListener> executorListenerMap = new ConcurrentHashMap<>();


  @Override
  public String submitTask(SingleTask singleTask) {
    List<IDataPipline> dataPiplines = piplineMap.get(singleTask.getTaskId());
    if (dataPiplines != null) {
      throw new PiplineCreateException("该任务号已经下发！");
    }
    ExecutorListener executorListener = new ExecutorListener(singleTask.getTaskId(), client, pluginServer.getNodeInfo());
    executorListenerMap.put(singleTask.getTaskId(), executorListener);
    try {
      IDataSinkCreator dataSinkCreator = getDataSinkCreator(singleTask);
      dataPiplines = new ArrayList<>();
      List<AbstractDataExecutor> executors = new ArrayList<>();
      doCreatePipline(0, singleTask.getNumberOfPipline(), singleTask, dataSinkCreator, dataPiplines, executors, executorListener);
      piplineMap.put(singleTask.getTaskId(), dataPiplines);
      executorMap.put(singleTask.getTaskId(), executors);
      taskStateMap.put(singleTask.getTaskId(), TaskState.created);
      taskMap.put(singleTask.getTaskId(), singleTask);
      executorListener.onPiplineCreate(0, singleTask.getNumberOfPipline(), null);
    } catch (Throwable throwable) {
      executorListener.onPiplineCreate(0, singleTask.getNumberOfPipline(), throwable);
      throw throwable;
    }

    return singleTask.getTaskId();
  }

  @NotNull
  private IDataSinkCreator getDataSinkCreator(SingleTask singleTask) {
    if (StringUtils.isBlank(singleTask.getTaskId()) || singleTask.getNumberOfPipline() < 0 || StringUtils.isBlank(singleTask.getPluginId()) || StringUtils.isBlank(singleTask.getMydataharborCreatorClazz()) || StringUtils.isBlank(singleTask.getConfigJson()) || StringUtils.isBlank(singleTask.getSettingJsonConfig())) {
      throw new PiplineCreateException("各参数不能为空或者为0校验不通过，请检查");
    }
    Map<String, IDataSinkCreator> stringIDataSinkCreatorMap = pluginInfoManager.getDataSinkCreatorMapByPlugin(singleTask.getPluginId());
    if (stringIDataSinkCreatorMap == null) {
      throw new PiplineCreateException("该机器没有安装这个插件，或者这个插件没有任何creator：" + singleTask.getPluginId());
    }
    IDataSinkCreator dataSinkCreator = stringIDataSinkCreatorMap.get(singleTask.getMydataharborCreatorClazz());
    if (dataSinkCreator == null) {
      throw new PiplineCreateException("没有这个creator：" + singleTask.getMydataharborCreatorClazz());
    }
    return dataSinkCreator;
  }

  private void doCreatePipline(int begin, int number, SingleTask singleTask, IDataSinkCreator dataSinkCreator, List<IDataPipline> dataPiplines, List<AbstractDataExecutor> executors, IExecutorListener executorListener) {
    ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
    try {
      Thread.currentThread().setContextClassLoader(dataSinkCreator.getClass().getClassLoader());
      for (int i = begin; i < number + begin; i++) {
        log.info("任务号:{},第{}个pipline开始创建", singleTask.getTaskId(), i + 1);
        IDataPipline dataPipline = dataSinkCreator.createPipline(dataSinkCreator.parseJson(singleTask.getConfigJson(), dataSinkCreator.getConfigClass()), (BaseSettingContext) dataSinkCreator.parseJson(singleTask.getSettingJsonConfig(), dataSinkCreator.getSettingClass()));
        generateRateGroup(singleTask, dataPipline);
        log.info("任务号:{},第{}个pipline创建完毕:{}", singleTask.getTaskId(), i + 1, dataPipline);
        dataPiplines.add(dataPipline);
        log.info("开始创建executor");
        Class<? extends AbstractDataExecutor> pointExecutorType = dataPipline.pointExecutorType();
        log.debug("使用+" + pointExecutorType + "创建");
        try {
          Constructor<? extends AbstractDataExecutor> constructor = pointExecutorType.getConstructor(IDataPipline.class, String.class);
          AbstractDataExecutor abstractDataExecutor = constructor.newInstance(dataPipline, dataPipline.threadNameGenerate(singleTask.getTaskId(), i + 1));
          abstractDataExecutor.addListener(executorListener);
          abstractDataExecutor.setTaskMonitorMBean(new TaskExecutorMonitor(singleTask.getTaskId(), abstractDataExecutor.getName(), null));
          executors.add(abstractDataExecutor);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
          throw new PiplineCreateException("pipline指定的executor执行器构造方法不符合规范:" + e.getMessage(), e);
        }
        log.info("executor创建完毕！");
      }
    } catch (Exception e) {
      log.error("依据参数创建pipline发送异常！", e);
      throw new PiplineCreateException("依据参数创建pipline发生异常:" + e.getMessage(), e);
    } finally {
      Thread.currentThread().setContextClassLoader(contextClassLoader);
    }
  }

  /**
   * 设置限速组
   *
   * @param singleTask
   * @param dataPipline
   */
  private void generateRateGroup(SingleTask singleTask, IDataPipline dataPipline) {
    if (dataPipline.dataSource() instanceof AbstractRateLimitDataSource) {
      AbstractRateLimitDataSource abstractRateLimitDataSource = (AbstractRateLimitDataSource) dataPipline.dataSource();
      RateLimitConfig rateLimit = abstractRateLimitDataSource.getRateLimitConfig();
      if (StringUtils.isBlank(rateLimit.getRateGroup())) {
        rateLimit.setRateGroup(singleTask.getTaskId());
      }
    }
  }

  @Override
  public TaskState queryTaskState(String taskId) {
    return taskStateMap.get(taskId);
  }

  @Override
  public List<SingleTask> lisTask() {
    return new ArrayList<>(taskMap.values());
  }

  @Override
  public void manageTask(String taskId, TaskState taskState) {
    TaskState nowState = this.taskStateMap.get(taskId);
    if (nowState == null)
      return;
    if (nowState.equals(taskState))
      return;
    List<AbstractDataExecutor> executors = executorMap.get(taskId);
    if (executors == null) {
      throw new TaskManageException("该task id 还没有创建pipline，无法管理状态");
    }
    switch (taskState) {
      case created:
        //创建
        throw new TaskManageException("创建请使用createPipline方法");
      case started:
        if (nowState != TaskState.created) {
          throw new TaskManageException("开始前状态不合法，当前状态：" + nowState + "期望：" + TaskState.created);
        }
        //启动
        for (AbstractDataExecutor executor : executors) {
          executor.start();
        }
        taskStateMap.put(taskId, TaskState.started);
        break;
      case suspend:
        if (nowState != TaskState.started) {
          throw new TaskManageException("当前状态不是正在运行，无法暂停");
        }
        for (AbstractDataExecutor executor : executors) {
          executor.pause();
        }
        taskStateMap.put(taskId, TaskState.suspend);
        break;
      case continued:
        if (nowState != TaskState.suspend && nowState != TaskState.started) {
          throw new TaskManageException("当前状态不是暂停或者运行，无法执行继续操作");
        }
        for (AbstractDataExecutor executor : executors) {
          executor.doContinue();
        }
        taskStateMap.put(taskId, TaskState.started);
        break;

      case over:
        //任何状态下都可以直接销毁
        for (AbstractDataExecutor executor : executors) {
          try {
            executor.close();
          } catch (IOException e) {
            log.error("调用pipline close方法发生异常，准备强制结束线程", e);
            try {
              executor.stop();
            } catch (Throwable throwable) {
              log.error("强行结束线程发生异常", e);
            }
          }
        }
        //保留状态
        taskStateMap.put(taskId, TaskState.over);
        //移除允许再次创建
        piplineMap.remove(taskId);
        executorMap.remove(taskId);
        break;
    }
    //更新task状态
    taskMap.get(taskId).setTaskState(taskState);
  }

  @Override
  public void pauseAndStart(String taskId) {
    TaskState nowState = this.taskStateMap.get(taskId);
    if (nowState != TaskState.created) {
      throw new TaskManageException("必须是初始状态才能执行该方法！");
    }
    List<AbstractDataExecutor> executors = executorMap.get(taskId);
    if (executors == null || executors.size() == 0) {
      throw new TaskManageException("没有该任务");
    }
    for (AbstractDataExecutor executor : executors) {
      executor.pause();
      executor.start();
    }
    taskStateMap.put(taskId, TaskState.suspend);
  }

  @Override
  public void editTaskNum(String taskId, Integer numberOfPipline, SingleTask newSingleTask) {
    List<AbstractDataExecutor> abstractDataExecutors = executorMap.get(taskId);
    if (abstractDataExecutors.size() == numberOfPipline) {
      log.info("任务数相等无需修改");
      return;
    } else if (abstractDataExecutors.size() > numberOfPipline) {
      //调小
      int change = abstractDataExecutors.size() - numberOfPipline;
      changeSmall(taskId, change, abstractDataExecutors);
    } else {
      //调大，比较复杂，要关注状态！
      int change = numberOfPipline - abstractDataExecutors.size();
      changeLarge(taskId, newSingleTask, change);
    }
    taskMap.get(taskId).setNumberOfPipline(numberOfPipline);
  }

  /**
   * 调大
   *
   * @param taskId
   * @param newSingleTask
   * @param change
   */
  private void changeLarge(String taskId, SingleTask newSingleTask, int change) {
    int beigin = executorMap.get(taskId).size();
    List<IDataPipline> dataPiplineChanges = new ArrayList<>();
    List<AbstractDataExecutor> executorChanges = new ArrayList<>();
    IExecutorListener executorListener = executorListenerMap.get(newSingleTask.getTaskId());
    try {
      IDataSinkCreator dataSinkCreator = getDataSinkCreator(newSingleTask);
      doCreatePipline(executorMap.get(taskId).size(), change, newSingleTask, dataSinkCreator, dataPiplineChanges, executorChanges, executorListener);
      piplineMap.get(taskId).addAll(dataPiplineChanges);
      executorMap.get(taskId).addAll(executorChanges);
      executorListener.onPiplineCreate(beigin, change, null);
    } catch (Throwable throwable) {
      executorListener.onPiplineCreate(beigin, change, throwable);
      throw throwable;
    }
    //获取当前状态
    TaskState taskState = taskStateMap.get(taskId);
    switch (taskState) {
      case created:
        //啥也不用做
        break;
      case started:
        //启动
        for (AbstractDataExecutor executor : executorChanges) {
          executor.start();
        }
        break;
      case suspend:
        for (AbstractDataExecutor executor : executorChanges) {
          executor.pause();
          executor.start();
        }
        break;
      case continued:
        for (AbstractDataExecutor executor : executorChanges) {
          executor.doContinue();
          executor.start();
        }
        break;
      case over:
        //不应该执行到这里，destory状态的task不能修改
        log.error("不应该执行到这里，destory状态的task不能修改:{}", newSingleTask);
        break;
    }
  }

  /**
   * 线程数调小
   *
   * @param taskId
   * @param change
   * @param abstractDataExecutors
   */
  private void changeSmall(String taskId, Integer change, List<AbstractDataExecutor> abstractDataExecutors) {
    List<IDataPipline> dataPiplines = piplineMap.get(taskId);
    //逆序删除
    ListIterator<AbstractDataExecutor> iterator = abstractDataExecutors.listIterator(abstractDataExecutors.size());
    int count = 1;
    while (iterator.hasPrevious()) {
      if (count > change)
        break;
      AbstractDataExecutor executor = iterator.previous();
      //关闭
      try {
        executor.close();
      } catch (IOException e) {
        log.error("调整pipline数时，优雅关闭执行器失败！", e);
        try {
          executor.stop();
        } catch (Throwable throwable) {
          log.error("强行结束线程发生异常", e);
        }
      } finally {
        dataPiplines.remove(executor.getDataPipline());
        iterator.remove();
        count++;
      }
    }
  }


}